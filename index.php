<?php
/**
 * index.php
 *
 * @author Jeff Peralta - SiniMed Pte Ltd - jeff.peralta@sinimed.com
 * @copyright  2018 SiniMed Pte Ltd
 * Date: 23/2/2018
 * Time: 10:51 AM
 *
 * LICENSE: This source file is the property of SiniMed Pte Ltd
 */
include_once ('list.php');
?>
<html>
    <head>
      <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div style="text-align: center">
            <h1>Launcher Tracker</h1>
            <h5>Click on the column to sort</h5>
        </div>
        <div style="width: 100%; text-align: center">
            <div class="datagrid">
                <table>
                    <thead>
                        <tr>
                            <th><a href="index.php?sort_by=id&sort_dir=<?php echo $next_sort_dir;?>">ID</a></th>
                            <th><a href="index.php?sort_by=name&sort_dir=<?php echo $next_sort_dir;?>">Name</a></th>
                            <th><a href="index.php?sort_by=time&sort_dir=<?php echo $next_sort_dir;?>">Datetime</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i = 0;
                            foreach($computers as $id => $computer){
                                $class = ($i++ % 2 ? "" : 'class="alt"');
                                echo "<tr $class><td>{$computer['id']}</td><td>{$computer['name']}</td><td>{$computer['time']}</td></tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </body>
</html>





