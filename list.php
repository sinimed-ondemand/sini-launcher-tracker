<?php
/**
 * list.php
 *
 * @author Jeff Peralta - SiniMed Pte Ltd - jeff.peralta@sinimed.com
 * @copyright  2018 SiniMed Pte Ltd
 * Date: 23/2/2018
 * Time: 1:38 PM
 *
 * LICENSE: This source file is the property of SiniMed Pte Ltd
 */

require_once("config.php");

$sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'none';
$sort_dir = isset($_GET['sort_dir']) ? $_GET['sort_dir'] : 'asc';
$next_sort_dir = (strtolower($sort_dir) == 'asc'? 'desc':'asc');

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row) $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}



$data = json_decode(file_get_contents($file_name_data), true);

$computers = array();
$handle = fopen($file_name_ini, "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        $temp = explode(";",$line);
        if(isset($temp[0])) {
            $temp = explode("=",$temp[0]);
            $computer_name = $temp[0];
            $computer_id = $temp[1];
            $computers[$computer_id] = array(
                 "id" => $computer_id
                ,"name" => $computer_name
                ,"time" => (isset($data[$computer_id]['SLOGTIME']) ? $data[$computer_id]['SLOGTIME'] : "")
            );
        }
    }

    if($sort_by != 'none') {
        $computers = array_orderby($computers, $sort_by, (strtolower($sort_dir)=="asc"?SORT_ASC:SORT_DESC));
    }

    fclose($handle);
} else {
    echo "<pre>Computer ini file cannot be read<br />";
}
