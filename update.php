<?php
/**
 * update.php
 *
 * @author Jeff Peralta - SiniMed Pte Ltd - jeff.peralta@sinimed.com
 * @copyright  2018 SiniMed Pte Ltd
 * Date: 23/2/2018
 * Time: 11:11 AM
 *
 * LICENSE: This source file is the property of SiniMed Pte Ltd
 */
require_once("config.php");

if( $conn = sqlsrv_connect($server, $connection_info) ) {
    $query = sqlsrv_query($conn, $query_string);
    $results = array();
    while ($row = sqlsrv_fetch_object  ($query)) {
        $row->SLOGTIME = $row->SLOGTIME->format('Y-m-d H:i:s');
        if($computer_unique_key != "") {
            $results[$row->$computer_unique_key] = (array) $row;
        }else{
            $results[] = (array) $row;
        }
    }

    $data = file_get_contents($file_name_data);
    if($data != "") {
        $data = json_decode($data, true);
        $data = array_merge($data, $results);
    }else{
        $data = $results;
    }

    $data = json_encode($data);
    file_put_contents($file_name_data, $data);

}else{
    echo "<pre>Connection could not be established.<br />";
    die( print_r( sqlsrv_errors(), true));
}

sqlsrv_close ($conn);
echo "ok";